@isTest(isParallel=true)
public without sharing class BulkMergeTest {

	public static final Integer DML_LIMIT = Limits.getLimitDmlStatements();

	@isTest
	public static void testValidateValid() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		objects1.add(new Lead(LastName = 'Someone'));
		objects1.add(new Lead(LastName = 'Someone else'));
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		objects2.add(new Account(Name = 'Acme, Inc.'));
		objects2.add(new Account(Name = 'ACME, Inc'));
		payload.put(new Account(Name = 'ACME'), objects2);

		Test.startTest();
		Boolean result = BulkMerge.validate(payload);
		Test.stopTest();

		System.assertEquals(true, result, 'Bulkmerge.validate() does not detect that all objects in a payload component are of the same type.');
	}

	@isTest
	public static void testValidateInvalid() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		objects1.add(new Lead(LastName = 'Someone'));
		objects1.add(new Lead(LastName = 'Someone else'));
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		objects2.add(new Account(Name = 'Acme, Inc.'));
		objects2.add(new Account(Name = 'ACME, Inc'));
		payload.put(new Lead(LastName = 'ACME'), objects2);

		Test.startTest();
		Boolean result = BulkMerge.validate(payload);
		Test.stopTest();

		System.assertEquals(false, result, 'Bulkmerge.validate() does not detect that all objects in a payload component are not of the same type.');
	}

	@isTest
	public static void testValidateEmpty() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		payload.put(new Account(Name = 'ACME'), objects2);

		Test.startTest();
		Boolean result = BulkMerge.validate(payload);
		Test.stopTest();

		System.assertEquals(true, result, 'Bulkmerge.validate() does not consider valid payload components without duplicates.');
	}

	@isTest
	public static void testValidateWrongType() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		payload.put(new Campaign(Name = 'ACME'), objects2);

		Test.startTest();
		Boolean result = BulkMerge.validate(payload);
		Test.stopTest();

		System.assertEquals(false, result, 'Bulkmerge.validate() does not reject objects that cannot be merged.');
	}

	@isTest
	public static void testRunTypeMismatch() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		objects1.add(new Lead(LastName = 'Someone'));
		objects1.add(new Lead(LastName = 'Someone else'));
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		objects2.add(new Account(Name = 'Acme, Inc.'));
		objects2.add(new Account(Name = 'ACME, Inc'));
		payload.put(new Lead(LastName = 'ACME'), objects2);

		Test.startTest();
		try {
			BulkMerge.run(payload);
			System.assert(false, 'Bulkmerge.run() did not throw an exception upon an invalid payload.');
		} catch (Exception e) {
			System.assert(e.getMessage().contains(BulkMerge.ERROR_INVALID_PAYLOAD), 'Bulkmerge.run() threw an error different than the one that was expecteed.');
		}
		Test.stopTest();
	}

	@isTest
	public static void testRunWrongType() {
		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		
		List<SObject> objects1 = new List<SObject>();
		objects1.add(new Lead(LastName = 'Someone'));
		objects1.add(new Lead(LastName = 'Someone else'));
		payload.put(new Lead(LastName = 'Someone important'), objects1);

		List<SObject> objects2 = new List<SObject>();
		objects2.add(new Campaign(Name = 'Campaign One'));
		objects2.add(new Campaign(Name = 'Campaign Uno'));
		payload.put(new Campaign(Name = 'Campaign 1'), objects2);

		Test.startTest();
		try {
			BulkMerge.run(payload);
			System.assert(false, 'Bulkmerge.run() did not throw an exception upon an invalid payload.');
		} catch (Exception e) {
			System.assert(e.getMessage().contains(BulkMerge.ERROR_INVALID_PAYLOAD), 'Bulkmerge.run() threw an error different than the one that was expecteed.');
		}
		Test.stopTest();
	}

	@isTest
	public static void testMergeDepthOneDuplicate() {
		testMergeDepthSeveralDuplicates(1);
	}

	@isTest
	public static void testMergeDepthTwoDuplicates() {
		testMergeDepthSeveralDuplicates(2);
	}

	@isTest
	public static void testMergeDepthThreeDuplicates() {
		testMergeDepthSeveralDuplicates(3);
	}

	@isTest
	public static void testMergeDepthOneDmlLimit() {
		testMergeDepthSeveralDuplicates(DML_LIMIT*2);
	}

	@SuppressWarnings('PMD.ApexUnitTestMethodShouldHaveIsTestAnnotation')
	public static void testMergeDepthSeveralDuplicates(Integer howMany) {
		Account master = new Account(Name = 'Acme');

		List<Account> duplicates = new List<Account>();
		for (Integer i = 0; i < howMany; i++) {
			duplicates.add(new Account(Name = 'ACME'));
		}

		insert master;
		insert duplicates;

		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		payload.put(master, duplicates);

		Test.startTest();
		BulkMerge.run(payload);
		Test.stopTest();

		master = [SELECT Id, IsDeleted, MasterRecordId FROM Account WHERE Id = :master.Id LIMIT 1 ALL ROWS];
		System.assertEquals(false, master.IsDeleted, 'The master record got deleted.');
		System.assertEquals(null, master.MasterRecordId, 'The master record was associated to another record as a master.');

		duplicates = [SELECT Id, IsDeleted, MasterRecordId FROM Account WHERE Id IN :duplicates ALL ROWS];

		for (Account thisDuplicate: duplicates) {
			System.assertEquals(true, thisDuplicate.IsDeleted, 'The duplicate record did not get deleted.');
			System.assertEquals(master.Id, thisDuplicate.MasterRecordId, 'The duplicate record was not associated to the master.');
		}
	}

	@isTest
	public static void testMergeWidthOneMaster() {
		testMergeWidthSeveralMasters(1);
	}

	@isTest
	public static void testMergeWidthTwoMasters() {
		testMergeWidthSeveralMasters(2);
	}

	@isTest
	public static void testMergeWidthThreeMasters() {
		testMergeWidthSeveralMasters(3);
	}

	@isTest
	public static void testMergeWidthOneDmlLimit() {
		testMergeWidthSeveralMasters(DML_LIMIT);
	}

	@SuppressWarnings('PMD.ApexUnitTestMethodShouldHaveIsTestAnnotation')
	public static void testMergeWidthSeveralMasters(Integer howMany) {
		List<Account> masters = new List<Account>();
		List<Account> duplicates = new List<Account>();
		for (Integer i = 0; i < howMany; i++) {
			masters.add(new Account(Name = 'Acme'));
			duplicates.add(new Account(Name = 'ACME'));
		}

		insert masters;
		insert duplicates;

		Map<SObject, List<SObject>> payload = new Map<SObject, List<SObject>>();
		for (Integer i = 0; i < howMany; i++) {
			List<Account> singleDuplicate = new List<Account>();
			singleDuplicate.add(duplicates[i]);
			payload.put(masters[i], singleDuplicate);
		}

		Test.startTest();
		BulkMerge.run(payload);
		Test.stopTest();

		masters = [SELECT Id, IsDeleted, MasterRecordId FROM Account WHERE Id IN :masters ALL ROWS];
		Set<Id> masterIds = new Set<Id>();

		for (Account thisMaster: masters) {
			System.assertEquals(false, thisMaster.IsDeleted, 'The master record got deleted.');
			System.assertEquals(null, thisMaster.MasterRecordId, 'The master record was associated to another record as a master.');
			masterIds.add(thisMaster.Id);
		}

		duplicates = [SELECT Id, IsDeleted, MasterRecordId FROM Account WHERE Id IN :duplicates ALL ROWS];

		for (Account thisDuplicate: duplicates) {
			System.assertEquals(true, thisDuplicate.IsDeleted, 'The duplicate record did not get deleted.');
			System.assert(masterIds.contains(thisDuplicate.MasterRecordId), 'The duplicate record was not associated to a master.');
		}
	}


}
